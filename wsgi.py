from app import app as application
import os

application.secret_key = os.urandom(12)

if __name__ == "__main__":

    application.run()