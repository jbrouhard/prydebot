#! /bin/sh

# Start the flask app, then the bot and then finally the cron, all being daemonized.

./app.py &

echo "Flask App Started"

./cron.py &

echo "cron started.  Happy Twitching!"

