#!/usr/bin/env python3

from flask import Flask, render_template, flash, redirect, request, session, send_from_directory, url_for
import os, logging, subprocess, urllib.request, csv
from config.config import *
from pathlib import Path
from werkzeug.utils import secure_filename
from lib.functions_general import *
from lib.streamelementsapi import *
from lib.decapi import *

UPLOAD_FOLDER = 'import/'
ALLOWED_EXTENSIONS = set(['csv'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


#logging.basicConfig(filename='app.log', filemode='w', level=logging.DEBUG, format='%(asctime)s %(message)s')   # Uncomment this to enable logging


def get_pid():
    logging.info('Updating Flask App PID')
    pid = os.getpid()
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("UPDATE settings SET value = %s WHERE param = 'config.app.pid'" % pid)
    conn.commit()
    conn.close()


@app.route('/setup')
def setup():
    if config_bot_license == 'True':
        return redirect('/', code=302)
    else:
        return render_template('setup.html')


# Lets execute the setup and put all data into the database
@app.route('/setup2', methods=['GET', 'POST'])
def do_setup():
    # Lets take the form variables and put them into query
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    username = request.form['username']
    password = request.form['password']
    password = bcrypt.hashpw(password.encode('UTF-8'), bcrypt.gensalt()).decode('utf-8') # Hash the password for security
    botname = request.form['botname']
    oauth = request.form['oauth']
    channel = "#" + username
    c.execute("UPDATE settings SET value = '%s' WHERE param = 'twitch.username'" % botname)
    c.execute("UPDATE settings SET value = '%s' WHERE param = 'twitch.oauth'" % oauth)
    c.execute('INSERT INTO editors (username, password, permissions) VALUES ("%s", "%s", "admin")' % (username, password))
    c.execute("UPDATE settings SET value = '%s' WHERE param = 'twitch.channel'" % channel)
    c.execute("UPDATE settings SET value = 'True' WHERE param = 'bot.license'")
    conn.commit()
    conn.close()
    return redirect('/setup3', code=302)


@app.route('/setup3')
def finish_setup():
    return render_template('setup3.html')


# Main dashboard

@app.route('/')
def home():
    if config_bot_license == "True":
        if not session.get('logged_in'):
            return render_template('login.html')
        else:
            channel = config['channel'][1:]

            conn = sql.connect(db_host, db_uname, db_pass, db_name)
            c = conn.cursor(sql.cursors.DictCursor)
            conn.ping(reconnect=True)
            c.execute("SELECT * from kwraffle_config")
            raffles = c.fetchall()
            conn.close()

            return render_template('dashboard.html', channel=channel, raffles=raffles)

    else:
        return redirect('/setup', code=302)


# Editor profile page

@app.route('/profile')
def profile():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        username = session.get('username')
        return render_template("profile.html", **locals())


@app.route('/profile/edit/<username>', methods=['GET', 'POST'])
def edit_profile(username):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        username = username
        password = request.form['password']
        password = bcrypt.hashpw(password.encode('UTF-8'), bcrypt.gensalt()).decode('utf-8')  # Hash the password for security
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute('UPDATE editors SET password = "%s" WHERE username = "%s"' % (password, username))
        conn.commit()
        conn.close()
        return redirect('/profile', code=302)


# API Endpoints for internal stuff

@app.route('/api/data/follower')
def api_data_follower():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT * FROM channel_data WHERE param = 'follower'")
    for row in c:
        data = row['value']
    conn.close()
    return render_template('apidata.html', **locals())


@app.route('/api/data/follows')
def api_data_follows():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT * FROM channel_data WHERE param = 'total_followers'")
    for row in c:
        data = row['value']
    conn.close()
    return render_template('apidata.html', **locals())


@app.route('/api/data/sub')
def api_data_sub():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT * FROM channel_data WHERE param = 'sub'")
    for row in c:
        data = row['value']
    conn.close()
    return render_template('apidata.html', **locals())


@app.route('/api/data/subs')
def api_data_subs():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT * FROM channel_data WHERE param = 'total_subs'")
    for row in c:
        data = row['value']
    conn.close()
    return render_template('apidata.html', **locals())


@app.route('/api/data/uptime')
def api_data_uptime():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT * FROM channel_data WHERE param = 'uptime'")
    for row in c:
        data = row['value']
    conn.close()
    return render_template('apidata.html', **locals())


@app.route('/api/data/viewers')
def api_data_viewers():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT * FROM channel_data WHERE param = 'viewers'")
    for row in c:
        data = row['value']
    conn.close()
    return render_template('apidata.html', **locals())


@app.route('/api/data/botstatus')
def api_data_bot():
    pid = botstatus()
    if pid_exists(pid) is True:
        data = "Online"
    else:
        data = "Offline"
    return render_template('apidata.html', **locals())


@app.route('/api/data/cronstatus')
def api_data_cron():
    pid = cronstatus()
    if pid_exists(pid) is True:
        data = "Online"
    else:
        data = "Offline"
    return render_template('apidata.html', **locals())


@app.route('/api/data/rafflestatus')
def api_data_raffle():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT * FROM settings WHERE param = 'config.kwraffle.service'")
    for row in c:
        data = row['value']
        conn.close()
        return render_template('apidata.html', **locals())


@app.route('/api/data/rafflelist')
def api_data_rafflelist():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT * FROM kwraffle_config")
    result = c.fetchall()
    return render_template('resultapi.html', **locals())


@app.route('/api/data/raffle/<keyword>')
def api_data_raffleusers(keyword):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT * FROM raffle WHERE keyword = '%s'" % keyword)
    result = c.fetchall()
    return render_template('raffleapiusers.html', result=result)


@app.route('/api/data/winner/<keyword>')
def api_data_rafflewinner(keyword):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT * FROM winners WHERE keyword = '%s'" % keyword)
    for row in c:
        data = row['username']
    return render_template('apidata.html', **locals())


# Service endpoints

@app.route('/service/bot')
def bot_service():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        pid = botstatus()
        pid = int(pid)
        if pid_exists(pid) is True:
            p = psutil.Process(pid)
            p.terminate()
        elif pid_exists(pid) is False:
            #Popen(['server.py', '&'])
            subprocess.call("./bot.py &", shell=True)
    return redirect('/', code=302)


@app.route('/service/cron')
def cron_service():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        pid = cronstatus()
        pid = int(pid)
        if pid_exists(pid) is True:
            p = psutil.Process(pid)
            p.terminate()
        elif pid_exists(pid) is False:
            subprocess.call("./cron.py &", shell=True)
    return redirect('/', code=302)


# Logs endpoints

@app.route('/logs/app')
def app_log():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        logs = open('app.log', 'r').read()

        log_name = "Administrative"
        return render_template('logs.html', **locals())


@app.route('/logs/bot')
def bot_log():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        logs = open('bot.log', 'r').read()
        log_name = "Bot Service"
        return render_template('logs.html', **locals())


@app.route('/logs/cron')
def cron_log():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        logs = open('cron.log', 'r').read()
        log_name = "Cron Service"
        return render_template('logs.html', **locals())


# Custom command stuff

@app.route('/commands')
def commands():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT * FROM commands WHERE function IS NULL ORDER BY command asc")
        result = c.fetchall()
        conn.close()

        return render_template('commands.html', commands=result)


@app.route('/commands/add', methods=['GET', 'POST'])
def do_add_command():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    command = request.form['command']
    cooldown = request.form['cooldown']
    response = request.form['response'].replace("'", "''")
    c.execute("INSERT INTO commands (command, cooldown, value, argc, last_used) VALUES ('%s', '%s', '%s', 0, 0)" % (command, cooldown, response))
    conn.commit()
    conn.close()
    return redirect('/commands', code=302)


@app.route('/commands/edit/<id>', methods=['GET', 'POST'])
def do_edit_command(id):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    command = request.form['command']
    cooldown = request.form['cooldown']
    response = request.form['response'].replace("'", "''")
    c.execute("UPDATE commands SET command= '%s', cooldown= '%s', value= '%s' WHERE id= '%s'" % (command, cooldown, response, id))
    conn.commit()
    conn.close()
    return redirect('/commands', code=302)


@app.route('/commands/remove/<id>', methods=['GET', 'POST'])
def do_remove_command(id):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    cmdid = id
    v = (cmdid, )
    c.execute("DELETE FROM commands WHERE id = '%s'" % id)
    conn.commit()
    conn.close()
    return redirect('/commands', code=302)


# Command Aliasing

@app.route('/aliases')
def aliases():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT * FROM commands WHERE function = 'Slave' ORDER BY command asc")
        result = c.fetchall()
        conn.close()

        return render_template('aliases.html', commands=result)


@app.route('/alias/add', methods=['GET', 'POST'])
def do_add_alias():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    command = request.form['command']
    response = request.form['value']
    c.execute("INSERT INTO commands (command, cooldown, argc, last_used, value, function, status) VALUES ('%s', 0, 0, 0, '%s', 'Slave', 'Enabled')" % (command, response))
    conn.commit()
    conn.close()
    return redirect('/aliases', code=302)


@app.route('/alias/edit/<id>', methods=['GET', 'POST'])
def do_edit_alias(id):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    command = request.form['command']
    response = request.form['value']
    c.execute("UPDATE commands SET command = '%s', value = '%s', function = 'Slave' WHERE id = '%s'" % (command, response, id))
    conn.commit()
    conn.close()
    return redirect('/aliases', code=302)


@app.route('/alias/remove/<id>', methods=['GET', 'POST'])
def do_remove_alias(id):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("DELETE FROM commands WHERE id = '%s'"% id)
    conn.commit()
    conn.close()
    return redirect('/aliases', code=302)


# Points

@app.route('/points')
def points():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT * FROM points ORDER BY id asc")
        query = c.fetchall()
        conn.close()

        return render_template('points.html', points=query)


@app.route('/points/add', methods=['GET', 'POST'])
def do_add_points():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    user = request.form['user']
    points = request.form['points']
    c.execute("INSERT INTO points (user, points) VALUES ('%s', '%s')" % (user, points))
    conn.commit()
    conn.close()
    return redirect('/points', code=302)


@app.route('/points/edit/<id>', methods=['GET', 'POST'])
def do_edit_points(id):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    user = request.form['user']
    points = request.form['points']
    time = request.form['time']
    c.execute("UPDATE points SET user = '%s', points = '%s', time = '%s' WHERE id = '%s'" % (user, points, time, id))
    conn.commit()
    conn.close()
    return redirect('/points', code=302)


@app.route('/points/remove/<id>', methods=['GET', 'POST'])
def do_remove_points(id):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("DELETE FROM points WHERE id = '%s'" % id)
    conn.commit()
    conn.close()
    return redirect('/points', code=302)


# Ranks

@app.route('/ranks')
def rank():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT * FROM rank ORDER BY id asc")
        query = c.fetchall()
        conn.close()

        return render_template('ranks.html', rank=query)


@app.route('/rank/add', methods=['GET', 'POST'])
def do_add_rank():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    name = request.form['name']
    rfrom = request.form['from']
    rto = request.form['to']
    c.execute("INSERT INTO rank (rank_name, from, to) VALUES ('%s', '%s', '%s')" % (name, rfrom, rto))
    conn.commit()
    conn.close()
    return redirect('/ranks', code=302)


@app.route('/ranks/edit/<id>', methods=['GET', 'POST'])
def do_edit_rank(id):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    name = request.form['name']
    rfrom = request.form['from']
    rto = request.form['to']
    c.execute("UPDATE rank SET rank_name = '%s', `from` = '%s', `to` = '%s' WHERE id = '%s'" % (name, rfrom, rto, id))
    conn.commit()
    conn.close()
    return redirect('/ranks', code=302)


@app.route('/ranks/remove/<id>', methods=['GET', 'POST'])
def do_remove_rank(id):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("DELETE FROM rank WHERE id = '%s'" % id)
    conn.commit()
    conn.close()
    return redirect('/ranks', code=302)


# Quotes

@app.route('/quotes')
def quotes():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT id, quote, author, date FROM quotes ORDER BY id asc")
        query = c.fetchall()
        conn.close()

        return render_template('quotes.html', quotes=query)


@app.route('/quotes/add', methods=['GET', 'POST'])
def do_add_quotes():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    quote = request.form['quote'].replace("'", "''")
    author = request.form['author']
    c.execute("INSERT INTO quotes (quote, author, date) VALUES ('%s', '%s', NOW())" % (quote, author))
    conn.commit()
    conn.close()
    return redirect('/quotes', code=302)


@app.route('/quotes/edit/<id>', methods=['GET', 'POST'])
def do_edit_quotes(id):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    quote = request.form['quote']
    author = request.form['author']
    c.execute("UPDATE quotes SET quote = '%s', author = '%s', date = NOW() WHERE id = '%s'" % (quote, author, id))
    conn.commit()
    conn.close()
    return redirect('/quotes', code=302)


@app.route('/quotes/remove/<id>', methods=['GET', 'POST'])
def do_remove_quotes(id):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("DELETE FROM quotes WHERE id = '%s'"% id)
    conn.commit()
    conn.close()
    return redirect('/quotes', code=302)


# Raffle system

@app.route('/raffle')
def raffle():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT * from kwraffle_config")
        query = c.fetchall()
        conn.close()

        return render_template("raffle.html", query=query)


@app.route('/raffle/add', methods=['GET', 'POST'])
def add_raffle():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        keyword = request.form['keyword']
        try:
            request.form['follower_only']
        except KeyError:
            follower_only = "N"
        else:
            follower_only = "Y"

        try:
            request.form['sub_only']
        except KeyError:
            sub_only = "N"
        else:
            sub_only = "Y"

        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("INSERT INTO kwraffle_config (keyword, follower_only, sub_only) VALUES ('%s', '%s', '%s')"% (keyword, follower_only, sub_only))
        conn.commit()
        conn.close()

        return redirect("/raffle", code=302)


@app.route('/raffle/<keyword>')
def do_raffle(keyword):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT * FROM raffle WHERE keyword = '%s'" % keyword)
        query = c.fetchall()
        conn.close()

        return render_template('raffle_id.html', query=query, keyword=keyword)


@app.route('/raffle/activate/<keyword>')
def do_activate_raffle(keyword):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("UPDATE kwraffle_config SET active = 'Y', new = 'Y' WHERE keyword = '%s'" % keyword)
        conn.commit()
        conn.close()

        return redirect("/raffle", code=302)


@app.route('/raffle/delete/<keyword>')
def do_delete_raffle(keyword):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("delete from kwraffle_config WHERE keyword = '%s'" % keyword)
        conn.commit()
        conn.close()

        return redirect("/raffle", code=302)


@app.route('/raffle/<keyword>/delete/<username>', methods=['GET', 'POST'])
def del_raffle_user(keyword, username):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("DELETE FROM raffle WHERE keyword = '%s' AND username = '%s'" % (keyword, username))
        conn.commit()
        conn.close()

        return redirect("/raffle/%s" % keyword, code=302)


@app.route('/raffle/<keyword>/add', methods=['GET', 'POST'])
def add_raffle_user(keyword):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        username = request.form['username']
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("INSERT INTO raffle (keyword, username) VALUES ('%s', '%s')" % (keyword, username))
        conn.commit()
        conn.close()

        return redirect("/raffle/%s" % keyword, code=302)


@app.route('/raffle/winner/<keyword>', methods=['GET', 'POST'])
def raffle_winner(keyword):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT * FROM raffle WHERE keyword = '%s' ORDER BY RAND() LIMIT 1" % keyword)
        for row in c:
            winner = row['username']
            c.execute("INSERT INTO winners (keyword, username, announce) VALUES ('%s', '%s', 'y')" % (keyword, winner))
            conn.commit()
        conn.close()

        return redirect("/raffle/%s" % keyword, code=302)


@app.route('/raffle/clear/<keyword>', methods=['GET', 'POST'])
def clear_winner(keyword):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("DELETE FROM winners WHERE keyword = '%s'" % keyword)
        conn.commit()
        conn.close()

        return redirect("/raffle/%s" % keyword, code=302)


@app.route('/raffle/clearall/<keyword>', methods=['GET', 'POST'])
def clear_all(keyword):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("DELETE FROM raffle WHERE keyword = '%s'" % keyword)
        conn.commit()
        conn.close()

        return redirect("/raffle/%s" % keyword, code=302)


# Settings page (this will likely be resource heavy)

@app.route('/settings')
def settings():
    if not session.get('logged_in'):
        return render_template('login.html')
    elif session.get('perms') == 'editor':
        return redirect('/', code=302)
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT * FROM settings WHERE param != 'config.bot.ver' AND param != 'config.api.timer' AND param != 'points.event.timer' AND param NOT LIKE 'twitch.%'  AND param != 'event.cooldown.timer' AND param NOT LIKE 'se.%' ORDER BY param ASC")
        query = c.fetchall()
        conn.close()

        return render_template('settings.html', settings=query)


@app.route('/settings/twitch')
def settings_twitch():
    if not session.get('logged_in'):
        return render_template('login.html')
    elif session.get('perms') == 'editor':
        return redirect('/', code=302)
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT * FROM settings WHERE param LIKE 'twitch.%' OR param like 'se.%' ORDER BY param ASC")
        query = c.fetchall()
        conn.close()

        return render_template('twitch.html', settings=query)


@app.route('/settings/edit/<param>', methods=['POST', 'GET'])
def do_edit_settings(param):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    param = param
    value = request.form['value']
    v = (value, param)
    c.execute("UPDATE settings SET value = '%s' WHERE param = '%s'" % (value, param))
    conn.commit()
    conn.close()
    return redirect('/settings', code=302)


# Editors page (this will likely be resource heavy)

@app.route('/settings/editors')
def editors():
    if not session.get('logged_in'):
        return render_template('login.html')
    elif session.get('perms') == 'editor':
        return redirect('/', code=302)
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT * FROM editors WHERE permissions = 'editor' ORDER BY username ASC")
        query = c.fetchall()
        conn.close()

        return render_template('editors.html', editors=query)


@app.route('/editors/edit/<param>', methods=['POST', 'GET'])
def do_edit_editors(param):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    username = param
    password = request.form['password']
    password = bcrypt.hashpw(password.encode('UTF-8'), bcrypt.gensalt()).decode('utf-8')
    c.execute('UPDATE editors SET password = "%s" WHERE username = "%s"' % (password, username))
    conn.commit()
    conn.close()
    return redirect('/settings/editors', code=302)


@app.route('/editors/add', methods=['POST', 'GET'])
def do_add_editors():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    username = request.form['username']
    password = request.form['password']
    password = bcrypt.hashpw(password.encode('UTF-8'), bcrypt.gensalt()).decode('utf-8')
    c.execute('INSERT INTO editors (username, password, permissions) VALUES ("%s", "%s", "editor")' % (username, password))
    conn.commit()
    conn.close()
    return redirect('/settings/editors', code=302)


@app.route('/editors/remove/<param>', methods=['POST', 'GET'])
def do_remove_editors(param):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("DELETE FROM editors WHERE username = '%s'" % param)
    conn.commit()
    conn.close()
    return redirect('/settings/editors', code=302)


# Banned Words page

@app.route('/settings/bannedwords')
def words():
    if not session.get('logged_in'):
        return render_template('login.html')
    elif session.get('perms') == 'editor':
        return redirect('/', code=302)
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT * FROM bannedwords ORDER BY word ASC")
        query = c.fetchall()
        conn.close()

        return render_template('banned.html', words=query)


@app.route('/banned/add', methods=['POST', 'GET'])
def do_add_words():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    word = request.form['word']
    c.execute("INSERT INTO bannedwords (word) VALUES ('%s')" % word)
    conn.commit()
    conn.close()
    return redirect('/settings/bannedwords', code=302)


@app.route('/banned/remove/<param>', methods=['POST', 'GET'])
def do_remove_words(param):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("DELETE FROM bannedwords WHERE word = '%s'" % param)
    conn.commit()
    conn.close()
    return redirect('/settings/bannedwords', code=302)


# Modules page

@app.route('/settings/modules')
def modules():
    if not session.get('logged_in'):
        return render_template('login.html')
    elif session.get('perms') == 'editor':
        return redirect('/', code=302)
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("SELECT command, cooldown, function, status FROM commands WHERE function = 'True' ")
        query = c.fetchall()
        conn.close()

        return render_template('modules.html', modules=query)


@app.route('/settings/modules/add', methods=['POST', 'GET'])
def do_add_modules():
    if not session.get('logged_in'):
        return render_template('login.html')
    elif session.get('perms') == 'editor':
        return redirect('/', code=302)
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        m = request.form['module']
        # cd = request.form['cooldown'] (forcing all cooldowns to be 0 for now)
        c.execute('INSERT INTO commands (command, cooldown, argc, last_used, value, function, status) VALUES ("%s", "0", "0", "0", " ", "True", "Enabled")' % m)
        conn.commit()
        conn.close()
        return redirect('/settings/modules', code=302)


@app.route('/settings/modules/<param>', methods=['POST', 'GET'])
def do_set_modules(param):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    module = param
    value = request.form['status']
    c.execute("UPDATE commands SET status = '%s' WHERE command = '%s'" % (value, module))
    conn.commit()
    conn.close()
    return redirect('/settings/modules', code=302)


@app.route('/settings/modules/edit/<param>', methods=['POST', 'GET'])
def do_edit_modules(param):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    module = param
    cooldown = request.form['cooldown']
    c.execute("UPDATE commands SET cooldown = '%s' WHERE command = '%s'" % (cooldown, module))
    conn.commit()
    conn.close()
    return redirect('/settings/modules', code=302)


# Points import features
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/points/import')
def pointsimport():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        # Get a list of all importable files
        for line in os.listdir('import/'):
            file = line
            return render_template('import.html', **locals())


@app.route('/points/import/<file>')
def importpoints(file):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        dir_name = 'import/'
        filename = file
        file = Path(dir_name).joinpath(filename)
        with open(file) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            next(readCSV)
            for row in readCSV:
                # print(row[0],row[1],row[2],)
                c.execute("INSERT INTO points (user, points, time) VALUES ('%s', '%s', '%s')" % (row[0], row[1], row[2]))
                conn.commit()
        conn.close()
        return redirect('/points', code=302)


@app.route('/points/upload', methods=['POST', 'GET'])
def upload_file():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        if request.method == 'POST':
            # check if the post request has the file part
            if 'file' not in request.files:
                flash('No file part')
                return redirect(request.url)
            file = request.files['file']
            # if user does not select file, browser also
            # submit a empty part without filename
            if file.filename == '':
                flash('No selected file')
                return redirect(request.url)
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                return redirect('/points/import', code=302)
        return render_template('upload.html')


# About Page

@app.route('/about')
def about():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        query = config_bot_version
        return render_template('about.html', about=query)


# Login/logout routes

@app.route('/login', methods=['GET', 'POST'])
def do_admin_login():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    name = request.form['username']
    password = request.form['password']
    c.execute("SELECT * FROM editors WHERE username = '%s'" % name)
    for row in c:
        hashed_password = row['password']
        uname = row['username']
        perms = row['permissions']

    if bcrypt.checkpw(password.encode('utf-8'), hashed_password.encode('utf-8')) is True and uname == request.form['username']:
        session['logged_in'] = True
        session['perms'] = perms
        session['username'] = name

    else:
        flash('Wrong Password!')
    conn.close()
    return redirect('/', code=302)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    return redirect('/', code=302)


if __name__ == '__main__':
    get_pid()
    # main()
    app.secret_key = os.urandom(12)
    app.run(debug=True, host='0.0.0.0', port=8900)
