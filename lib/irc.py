import socket, re, sys
from lib.functions_general import *


class irc:
    def __init__(self, config):
        self.config = config

    def check_for_message(self, data):
        if re.match(r'^:[a-zA-Z0-9_]+\![a-zA-Z0-9_]+@[a-zA-Z0-9_]+(\.tmi\.twitch\.tv|\.testserver\.local) PRIVMSG #[a-zA-Z0-9_]+ :.+$', data.decode('utf-8')):
            return True

    def check_is_command(self, message, valid_commands):
        for command in valid_commands:
            if command == message:
                return True

    def check_for_connected(self, data):
        if re.match(r'^:.+ 001 .+ :connected to TMI$', data):
            return True

    def check_for_ping(self, data):
        if re.findall(r'PING :tmi.twitch.tv', data.decode('utf-8')):
            data = "PONG : tmi.twitch.tv\r\n"
            self.sock.send(data.encode('utf-8'))

    def get_message(self, data):
        return {
            'channel': re.findall(r'^:.+\![a-zA-Z0-9_]+@[a-zA-Z0-9_]+.+ PRIVMSG (.*?) :', data.decode('utf-8'))[0],
            'username': re.findall(r'^:([a-zA-Z0-9_]+)\!', data.decode('utf-8'))[0],
            'message': re.findall(r'PRIVMSG #[a-zA-Z0-9_]+ :(.+)', data.decode('utf-8'))[0]
        }

    def get_username(self, data):
        return {
            'username': re.findall(r'^:([a-zA-Z0-9_]+)\!', data.decode('utf-8'))[0]
        }

    def check_login_status(self, data):
        if re.match(b'^:(testserver\.local|tmi\.twitch\.tv) NOTICE \* :Login unsuccessful\r\n$', data):
            return False
        else:
            return True

    def send_message(self, channel, message):
        data = ('PRIVMSG %s :%s\n' % (channel, message))
        self.sock.send(data.encode('utf-8'))

    def get_irc_socket_object(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(10)

        self.sock = sock

        try:
            sock.connect((self.config['server'], self.config['port']))
        except:
            pp('Cannot connect to server (%s:%s).' % (self.config['server'], self.config['port']), 'error')
            sys.exit()

        sock.settimeout(None)

        user = 'USER %s\r\n' % self.config['botname']
        oauth = 'PASS %s\r\n' % self.config['oauth_password']
        nick = 'NICK %s\r\n' % self.config['botname']

        sock.send(user.encode('utf-8'))
        sock.send(oauth.encode('utf-8'))
        sock.send(nick.encode('utf-8'))

        if self.check_login_status(sock.recv(1024)):
            pp('Login successful.')
        else:
            pp('Login unsuccessful. (hint: make sure your oauth token is set in self.config/self.config.py).', 'error')
            sys.exit()

        self.join_channels(self.config['channel'])

        return sock

    def join_channels(self, channel):
        pp('Joining channels %s.' % channel)
        join = 'JOIN %s\r\n' % channel
        self.sock.send(join.encode('utf-8'))
        pp('Joined channels.')

    def leave_channels(self, channel):
        pp('Leaving chanels %s,' % channel)
        part = 'PART %s\r\n' % channel
        self.sock.send(part.encode('utf-8'))
        pp('Left channels.')
