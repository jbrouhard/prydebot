import importlib
import time
from config.config import *


def is_valid_command(command):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()
    conn.ping(reconnect=True)
    command = command.replace("'", "")  # Cause apostrophies cause the bot to murderzone itself
    c.execute("SELECT command FROM commands WHERE command = '%s'" % command)
    conn.close()
    if c.rowcount != 0:
        return True


def update_last_used(command):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()
    conn.ping(reconnect=True)
    c.execute("UPDATE commands SET last_used = %s WHERE command = '%s'" % (time.time(), command))
    conn.commit()
    conn.close()


def get_command_limit(command):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()
    conn.ping(reconnect=True)
    c.execute("SELECT command, cooldown FROM commands WHERE command = '%s'" % command)
    r = c.fetchone()
    conn.close()
    return r[1]


def is_on_cooldown(command):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()
    conn.ping(reconnect=True)
    c.execute("SELECT command, cooldown, last_used FROM commands WHERE command = '%s'" % command)
    r = c.fetchone()
    conn.close()
    if r is not None:
        last_used = r[2]
        limit = r[1]
        if time.time() - float(last_used) < limit:
            return True


def get_cooldown_remaining(command):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()
    conn.ping(reconnect=True)
    c.execute("SELECT command, cooldown, last_used FROM commands where command = '%s'" % command)
    r = c.fetchone()
    conn.close()
    last_used = r[2]
    limit = r[1]
    return float(limit) - (time.time() - float(last_used))


def check_has_return(command):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()
    conn.ping(reconnect=True)
    c.execute("SELECT command, value FROM commands where command = '%s'" % command)
    r = c.fetchone()
    conn.close()
    if r is not None:
        return True


def get_return(command):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()
    conn.ping(reconnect=True)
    c.execute("SELECT command, value FROM commands where command = '%s'" % command)
    r = c.fetchone()
    conn.close()
    if r is not None:
        return r[1]


def check_has_args(command):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()
    conn.ping(reconnect=True)
    c.execute("SELECT command, argc from commands where command = '%s'" % command)
    r = c.fetchone()
    conn.close()
    if r[1] == "True":
        return True


def check_has_correct_args(message, command):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()
    conn.ping(reconnect=True)
    c.execute("Select command, argc FROM commands WHERE command = '%s'" % command)
    r = c.fetchone()
    conn.close()
    if r is not None:
        return True


def check_returns_function(command):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()
    conn.ping(reconnect=True)
    c.execute("SELECT command, function, value, status FROM commands where command = '%s' AND status = 'Enabled'" % command)
    r = c.fetchone()
    conn.close()
    if r is not None:
        return True


def pass_to_function(username, command, args):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()
    conn.ping(reconnect=True)
    c.execute("SELECT command, function, value FROM commands where command = '%s'" % command)
    r = c.fetchone()
    conn.close()
    if r[1] == 'True':
        command = command.replace('!', '')

        module = importlib.import_module('lib.commands.%s' % command.lower())
        function = getattr(module, command.lower())

        if args:
            # need to reference lib.commands.<command
            return function(username, args)
        else:
            # need to reference lib.commands.<command
            return function(username)

    elif r[1] == 'Slave':  # Command is an alias
        command = r[2]
        command = command.replace('!', '')

        module = importlib.import_module('lib.commands.%s' % command.lower())
        function = getattr(module, command.lower())

        if args:
            # need to reference lib.commands.<command
            return function(username, args)
        else:
            # need to reference lib.commands.<command
            return function(username)
    else:
        # command has no module and is in database.  return it's value.
        return r[2]


def getUser(line):
    user = ""
    if line[1] == "PRIVMSG":
        user = line[0]
        user = user.split("!")
        user = user[0]
        user = user[1:]
    return user
