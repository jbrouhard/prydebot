"""
DecAPI.me Twitch API Python library

Developed by JonPryde <jp3dcustoms@gmail.com>
"""

from urllib.request import urlopen
from config.config import *

api = 'https://decapi.me/twitch/'


def getuptime():
    try:
        channel = config['channel'][1:]
        url = urlopen(api + 'uptime?channel=%s' % channel).read()
        data = url.decode()

        if data == channel + "is offline":
            data = "Offline"
            return data
        else:
            return data
    except:
        return None


def getViewrCount():
    try:
        channel = config['channel'][1:]
        url = urlopen(api + 'viewercount/%s' % channel).read()
        data = url.decode()

        if data == channel + "is offline":
            data = "Offline"
            return data
        else:
            return data
    except:
        return None

