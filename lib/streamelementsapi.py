"""
Streamelements API Python library

Developed by JonPryde <jp3dcustoms@gmail.com>
"""

import requests
from json import loads
from config.config import *                     # This is specific to Prydebot.  This can be removed as long as you set config['se.token'] somewhere.

api = 'https://api.streamelements.com/kappa/v2/'


def getrecenthost():
    try:
        session = requests.session()

        headers = {'Authorization': 'Bearer %s' % se_token}

        response = session.get(api + 'sessions/' + config['se.key'], headers=headers).text
        data = loads(response)
        data = data['data']
        data = data['host-latest']
        host = data['name']

        return host

    except:
        return None


def getrecentsub():
    try:
        session = requests.session()

        headers = {'Authorization': 'Bearer %s' % se_token}

        response = session.get(api + 'sessions/' + config['se.key'], headers=headers).text
        data = loads(response)

        data = data['data']
        recent_sub = data['subscriber-latest']
        recent_sub = recent_sub['name']

        return recent_sub

    except:
        return None


def gettotalsubs():
    try:
        session = requests.session()

        headers = {'Authorization': 'Bearer %s' % se_token}

        response = session.get(api + 'sessions/' + config['se.key'], headers=headers).text
        data = loads(response)

        data = data['data']
        data = data['subscriber-total']
        total_subs = data['count']

        return total_subs
    except:
        return None


def getrecentcheer():
    try:
        session = requests.session()

        headers = {'Authorization': 'Bearer %s' % se_token}

        response = session.get(api + 'sessions/' + config['se.key'], headers=headers).text
        data = loads(response)

        data = data['data']
        data = data['cheer-latest']
        recent_cheer = data['name']

        return recent_cheer

    except:
        return None


def getcheeramount():
    try:
        session = requests.session()

        headers = {'Authorization': 'Bearer %s' % se_token}

        response = session.get(api + 'sessions/' + config['se.key'], headers=headers).text
        data = loads(response)

        data = data['data']
        data = data['cheer-latest']
        cheer_amount = data['amount']

        return cheer_amount

    except:
        return None


def gettotalfollowers():
    try:
        session = requests.session()

        headers = {'Authorization': 'Bearer %s' % se_token}

        response = session.get(api + 'sessions/' + config['se.key'], headers=headers).text
        data = loads(response)

        data = data['data']
        data = data['follower-total']
        total_follows = data['count']

        return total_follows
    except:
        return None


def getrecentfollower():
    try:
        session = requests.session()

        headers = {'Authorization': 'Bearer %s' % se_token}

        response = session.get(api + 'sessions/' + config['se.key'], headers=headers).text
        data = loads(response)

        data = data['data']
        data = data['follower-latest']
        recent_follower = data['name']

        return recent_follower
    except:
        return None