import time, csv, bcrypt, psutil, requests, json
import sqlite3 as sql
from config.config import *
from urllib.request import urlopen
from json import loads
from pathlib import Path


red = "\033[01;31m{0}\033[00m"
grn = "\033[01;36m{0}\033[00m"
blu = "\033[01;34m{0}\033[00m"
cya = "\033[01;36m{0}\033[00m"


def pp(message, mtype='INFO'):
    mtype = mtype.upper()

    if mtype == "ERROR":
        mtype = red.format(mtype)

    print('[%s] [%s] %s' % (time.strftime('%H:%M:%S', time.gmtime()), mtype, message))


def ppi(channel, message, username):
    print(
        '[%s %s] <%s> %s' % (time.strftime('%H:%M:%S', time.gmtime()), channel, grn.format(username.lower()), message))


def pbot(message, channel=''):
    if channel:
        msg = '[%s %s] <%s> %s' % (time.strftime('%H:%M:%S', time.gmtime()), channel, red.format('BOT'), message)
    else:
        msg = '[%s] <%s> %s' % (time.strftime('%H:%M:%S', time.gmtime()), red.format('BOT'), message)

    print(msg)


# This will generate the password salting
def get_hashed_password(password):
    return bcrypt.hashpw(password.encode('UTF-8'), bcrypt.gensalt())


# This will check plaintext passwording against hashed.
def check_password(password, hashed_password):
    if hashed_password == bcrypt.hashpw(password.encode('utf-8'), hashed_password.encode('utf-8')):
        return True
    else:
        return False


# Check users' permission level
def permissions(username):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT username, permissions FROM editors WHERE permissions = 'caster' and username = '%s'" % username)
    query = c.fetchone()
    conn.close()
    if query is None:
        return True
    else:
        return False


# Get bot status
def botstatus():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT param, value FROM settings WHERE param = 'config.server.pid'")
    row = c.fetchone()
    return row['value']


# Get cron status
def cronstatus():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT param, value FROM settings WHERE param = 'config.cron.pid'")
    row = c.fetchone()
    return row['value']


def pid_exists(pid):
    pid = int(pid)
    return psutil.pid_exists(pid)


def boteditors(username):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    c.execute("SELECT * FROM editors WHERE username = '%s'" % username)
    c.fetchone()
    conn.close()
    if c.fetchone is None:
        return False


def regulars(username):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    c.execute("SELECT * FROM regulars WHERE name = '%s'" % username)
    query = c.fetchone()
    conn.close()
    if query is None:
        return False


def update_points(username, points):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    c.execute("SELECT user FROM points WHERE user = '%s'" % username)
    result = c.fetchone()
    if result:
        points = config_follow_points
        c.execute("UPDATE points SET points = points + %s WHERE user = '%s'" % (points, username))
        conn.commit()
    else:
        points = config_follow_points
        c.execute("INSERT INTO points (user, points, time) VALUES ('%s', '%s', '0')" % (username, points))
        conn.commit()
    conn.close()


def get_user_rank(points):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    c.execute("SELECT * FROM RANK WHERE from < $s AND to >= %s"%  (points, points))
    result=c.fetchone()
    if result:
        return result['rank']
    conn.close()


def get_channel_status():
    try:
        channel = config['channel'][1:]
        twitchapi = config['twitch.api.clientid']

        api = 'https://api.twitch.tv/kraken/streams/' + channel + '/?client_id=' + twitchapi

        response = urlopen(api)
        readable = response.read().decode('utf-8')
        stream = loads(readable)
        result = stream['stream']

        if result == 'Null':
            return True
        else:
            return False

    except:
        return True


def moderator(username):
    try:
        channel1 = config['channel'][1:]

        # call twitch's API to get list of users in channel
        api = 'https://tmi.twitch.tv/group/user/' + channel1 + '/chatters'

        response = urlopen(api)
        readable = response.read().decode('utf-8')
        chatlist = loads(readable)
        chatters = chatlist['chatters']
        mods = chatters['moderators']

        for i in mods:
            if username == i:
                return True
            else:
                return False

    except:
        return False


def permits(username):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()

    c.execute("Select * from permits WHERE username = '%s'" % username)
    result = c.fetchone()

    if result:
        return True
    else:
        return False


def points_import(file):
    dir_name = 'import/'
    csv = Path(dir_name).joinpath(file)
    with open(csv) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        next(readCSV)
        for row in readCSV:
            conn = sql.connect(db_host, db_uname, db_pass, db_name)
            c = conn.cursor(sql.cursors.DictCursor)
            # print(row[0],row[1],row[2],)
            c.execute('INSERT INTO POINTS (user, points, time) VALUES ("%s", "%s" "%s")' % (row[0], row[1], row[2]))
            conn.commit()
            conn.close()


def get_keyword(keyword):
    # Compare keyword to list of active keywords
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    c.execute('SELECT * FROM kwraffle_config WHERE keyword = "%s"' % keyword)
    result = c.fetchone()
    if result is not None:
        return True
    else:
        return False


