"""
Songrequest Module

Developed by JonPryde <jp3dcustoms@gmail.com>
"""

from config.config import *
from urllib.request import urlopen
import json


def songrequest(username, *args):
    #conn = sql.connect(db_host, db_uname, db_pass, db_name)
    #c = conn.cursor()

    if config['config_songrequest'] == "Disabled":
        resp = "Songrequest is disabled!"
        return resp

    else:

        if args:

            if config['config_songrequest'] == "Youtube":
                link = args[0]
                link = ''.join(link)
                id = link.rsplit('/', 1)[-1][8:]  # Strip the URL and first 8 characters from final link
                url = 'https://www.googleapis.com/youtube/v3/videos?part=snippet&id=%s&key=%s' % (id, config_google_api)

                yt = urlopen(url).read()
                data = json.loads(yt.decode())

                title = data['items'][0]['snippet']['title']

                resp = "%s requested song ' %s ' from %s" % (username, title, link)
                return resp

            elif config['config_songrequest'] == "Spotify":
                link = args[0]
                link = ''.join(link)
                resp = "%s requested song from %s" % (username, link)
                return resp

        else:
            resp = "Songrequest is enabled and you can use %s to request songs to be played on the stream!" % config['config_songrequest']
            return resp