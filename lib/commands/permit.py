"""
!permit  Module

Developed by JonPryde <jp3dcustoms@gmail.com>
"""

from config.config import *
from lib.functions_general import moderator


def permit(username, *args):
    # Check to make sure username is a part of the moderator team

    #if moderator(username) is not False:

        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor()

        # Lets dump the user into the permit list.  This is good for only *ONE* link!

        target = args[0]

        target = "".join(str(x) for x in target)

        c.execute('INSERT INTO permits (username) VALUES ("%s")' % target)
        conn.commit()

        resp = "%s you now have the ability to post ONE link to chat!" % target
        return resp