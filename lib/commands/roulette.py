"""
Russian Roulette Module

Developed by JonPryde <jp3dcustoms@gmail.com>
"""

import random

from bot import *

def roulette(username):
    choices = [
        "Click",
        "Click",
        "Click",
        "BANG!"
    ]

    result = random.choice(choices)

    if result == "BANG!":
        resp = "%s Spins a round in the chamber, cocks the hammer, holds the gun to their forehead and pulls the " \
               "trigger!!     **BANG**  You're dead %s!!!" % (username, username)
        return resp
        timeout = 5 # 5 seconds timeout - normal for most channels.
        send_str = "/timeout %s %s" % (username, timeout)
        pbot(send_str, channel)
        irc.send_message(channel, send_str)
    else:
        resp = "%s Spins a round in the chamber, cocks the hammer, holds the gun to their forehead and pulls the " \
               "trigger!! **CLICK**  You're safe, for now!" % username
        return resp
