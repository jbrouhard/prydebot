"""
Quotes Module

Developed by JonPryde <jp3dcustoms@gmail.com>
"""

from config.config import *


def quote(username, *args):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()

    if args:

        if len(args) == 1:
            qid = args[0]
            qid = ''.join(qid)

            c.execute("SELECT * FROM quotes WHERE id = '%s'" % qid)
            if c.rowcount != 0:
                for row in c:
                    resp = "Quote: Id: %s ' %s ' by %s on %s" % (row[0], row[1], row[2], row[3])
                    return resp

            else:
                resp = "Invalid quote"
                return resp

    else:
        c.execute("SELECT * FROM quotes ORDER BY RAND()")
        c.fetchone()
        for row in c:
            resp = "Random Quote: Id: %s ' %s ' by %s on %s" % (row[0], row[1], row[2], row[3])
            return resp

    conn.close()

