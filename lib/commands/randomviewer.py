from config.config import *

def randomviewer(username):
    from urllib.request import urlopen
    from json import loads
    import random


    # call twitch's API to get list of users in channel
    api = 'https://tmi.twitch.tv/group/user/'+config['channel'][1:]+'/chatters'

    response = urlopen(api)
    readable = response.read().decode('utf-8')
    chatlist = loads(readable)
    chatters = chatlist['chatters']
    viewers = chatters['viewers']
    randviewer = random.choice(list(viewers))

    # now lets get ready to send the response back to the channel.
    resp = 'The random viewer is %s' % randviewer
    return resp
