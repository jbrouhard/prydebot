"""
Commands help Module

Developed by JonPryde <jp3dcustoms@gmail.com>
"""

from config.config import *

def commands(username):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()

    # Lets get a list of all the enabled commands
    c.execute("SELECT command FROM commands ORDER BY command ASC")

    resp = list(c.fetchall())

    resp = '  '.join(str(r) for v in resp for r in v)

    return resp
