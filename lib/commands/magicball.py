"""
Points Module

Developed by JonPryde <jp3dcustoms@gmail.com>
"""

import random

usage = "You must ask a question for the 8ball to work!"


def magicball(username, *args):
    if args:
       bases = [
           "The answer lies in your heart"
            , "I do not know"
            , "Almost certainly"
            , "No"
            , "Instead, ask why you need to ask"
            , "Go away. I do not wish to answer at this time"
            , "Time will only tell"
            , "It is certain"
            , "it is decidedly so"
            , "Without a doubt"
            , "Yes definitely"
            , "You may rely on it"
            , "Most Likely"
            , "Outlook is good"
            , "Yes"
            , "Signs point to yes"
            , "Reply hazy.  Try again"
            , "Ask again later"
            , "Better not tell you now"
            , "Cannot predict now"
            , "Concentrate and ask again"
            , "Don't count on it"
            , "My reply is no"
            , "My sources say no"
            , "Outlook not so good"
            , "Very doubtful"
        ]

       suffixes = [
            "my child"
            , "young grasshopper"
            , "my young padawan"
            ,
        ]

       resp = '@' + username + ' ' + ', '.join([
            random.choice(bases)
            , random.choice(suffixes)
        ])

       return resp

    else:
       return usage

