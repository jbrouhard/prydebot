from urllib.request import urlopen
from config.config import *

def uptime(username):
    channel = config['channel'][1:]
    url = urlopen("https://decapi.me/twitch/uptime?channel="+channel+"").read()
    data = url.decode()
    resp = "@"+username+" channel has been live for " + data
    return resp