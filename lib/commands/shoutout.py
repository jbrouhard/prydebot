from config.config import *
from urllib.request import urlopen


def shoutout(username, *args):
    channel = args[0][0]
    url = urlopen("https://decapi.me/twitch/game/%s" % channel).read()
    game = url.decode()

    resp = "Go give %s a follow where they were last playing %s" % (channel, game)

    return resp