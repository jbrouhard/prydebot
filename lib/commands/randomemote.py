# coding: utf8

import random
import json


def randomemote(username):
    filename = 'res/global_emotes.json'

    try:
        with open(filename) as json_data:
            data = json.load(json_data)

    except:
        return 'Error reading %s.' % filename

    emote = random.choice(list(data.keys()))

    return '%s = %s' % (
        emote,
        emote[:1] + '​' + emote[1:]
    )
