"""
Points Module

Developed by JonPryde <jp3dcustoms@gmail.com>
"""

from lib.functions_general import *


def points(username, *args):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor()

    pointname = config['point_name']

    if args:
        # first lets make sure that the user has access (must be caster or editor in the bot)
        c.execute("SELECT * FROM editors WHERE username = '%s'" % username)
        c.fetchone()

        if c is not None:

            # Points come in as a list, but in a weird way.  So lets iterate through list and count the words.
            for i in args:
                num_words = len(i)

            usage = "!points <add/remove> <+viewers/username> <value>"     # usage when user doesn't access command correctly

            if num_words == 3:

                # get args and run the queries
                for i in args:
                    argc = i[0]
                    target = i[1]
                    point = i[2]

                if argc == "add":
                    if target == "+viewers":
                        channel = config['channel'][1:]
                        api = 'https://tmi.twitch.tv/group/user/' + channel + '/chatters'

                        response = urlopen(api)
                        readable = response.read().decode('utf-8')
                        chatlist = loads(readable)
                        chatters = chatlist['chatters']
                        viewers = chatters['viewers']
                        mods = chatters['moderators']

                        for i in viewers:
                            c.execute("UPDATE points SET points = points + '%s' WHERE user = '%s'" % (point, i))
                            conn.commit()

                        for i in mods:
                            c.execute("UPDATE points SET points = points + '%s' WHERE user = '%s'" % (point, i))
                            conn.commit()

                        resp = "Added %s %s to all viewers!!" % (point, pointname)
                        return resp

                    else:
                        c.execute("UPDATE points SET points = points + '%s' WHERE user = '%s'" % (point, target))
                        conn.commit()
                        resp = "Added %s %s to %s" % (point, pointname, target)
                        return resp

                elif argc == "remove":
                    c.execute("UPDATE points SET points = points - '%s' WHERE user = '%s'" % (point, target))
                    conn.commit()
                    resp = "Removed %s %s to %s" % (point, pointname, target)
                    return resp

            elif len(args) == 1:
                 user = args[0]
                 user = ''.join(user)
                 c.execute("SELECT points.user, points.points, points.time, rank.rank_name, rank.from, rank.to FROM points LEFT JOIN rank ON rank.from < points.points AND rank.to >= points.points WHERE user = '%s'" % user)
                 if c.rowcount != 0:
                     for row in c:
                         t = row[2] / 60
                         t = "{0:.2f}".format(t)
                         resp = "{0} has {1} {2}, their rank is {3} and have been watching for {4} hours!".format(
                             row[0], row[1], pointname, row[3], t)
                         return resp
                 else:
                     resp = "{0}, does not have any {1} in the system!".format(user, pointname)
                     return resp
            else:
               return usage

    else:
        c.execute("SELECT points.user, points.points, points.time, rank.rank_name, rank.from, rank.to FROM points LEFT JOIN rank ON rank.from < points.points AND rank.to >= points.points WHERE user = '%s'" % username)
        if c.rowcount != 0:
            for row in c:
                t = row[2] / 60
                t = "{0:.2f}".format(t)
                resp = "@{0} you have {1} {2}, your rank is {3} and have been watching for {4} hours!".format(row[0], row[1], pointname, row[3], t)
                return resp
        else:
            resp = "@{0}, you do not have any {1} in the system!".format(username, pointname)
            return resp

    conn.close()

