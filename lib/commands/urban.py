"""
Urban Dictionary search function

Developed by JonPryde <jp3dcustoms@gmail.com>
"""

from urllib.request import urlopen


def urban(username, *args):
    query = args[0]
    query = "".join(str(x) for x in query)
    response = urlopen("http://jwd.me/twitch/api/urban-dictionary.php?q="+query)
    resp = response.read().decode("utf-8")
    return resp