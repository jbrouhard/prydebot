from config.config import *


def raider(username, *args):
    if args:
        point = points_raider_award
        target = args[0][0]
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor()
        c.execute('SELECT * FROM points WHERE user = "%s"' % target)
        result = c.fetchone()
        if result:
            c.execute('UPDATE points SET points = points + "%s" WHERE user = "%s"' % (point, target))
            conn.commit()
        else:
            c.execute('INSERT INTO points (user, points) VALUES ("%s", "%s")' % (target, point))
            conn.commit()

        resp = "Thank you %s for the raid! %s %s have been awarded to you!" % (target, point, config_point_name)

        return resp

    else:
        resp = "You need to have a target!"

        return resp
