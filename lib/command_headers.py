# This file is no longer necessary.  Will remove from git when we get the command functions working

import sqlite3 as sql

db_filename = 'bot.db'

conn = sql.connect(db_filename, isolation_level=None)

c = conn.cursor()

commands = {}  # Since dictionary is going to be pouplated by the DB, create a blank one.

c.execute("SELECT * FROM commands")
c.row_factory = sql.Row # This is to ensure that the data is text, not binary.

# print all rows
for row in c:
    cmd = row['command']
    value = row['value']
    argc = row['argc']
    limit = row['limit']

    commands[cmd] = {}
    commands[cmd]['limit'] = limit
    commands[cmd]['argc'] = argc
    commands[cmd]['return'] = value


#print(commands) # To make sure dictionary is being printed correctly

# Old command stuff is below for archiving.
# commands = {
#    '!test': {
#        'limit': 30,
#        'return': 'This is a test!'
#    },
#    '!randomemote': {
#        'limit': 180,
#        'argc': 0,
#        'return': 'command'
#    },
#    '!wow': {
#        'limit': 30,
#        'argc': 3,
#        'return': 'command'
#    },
# }
#


