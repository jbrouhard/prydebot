#!/usr/bin/env python3

"""
PrydeBot - A Twitch Bot by JonPryde

https://gitlab.com/jbrouhard/prydebot
"""

from python_twitch_irc import TwitchIrc
from lib.functions_general import *
import lib.functions_commands as commands
from config.config import *
import time, logging, os
from urlextract import URLExtract
from concurrent.futures import ProcessPoolExecutor


# Lets do something simple first
class Bot(TwitchIrc):

    def on_connect(self):
        self.join('%s' % config['channel'])

    # We need to get the PID of this, and dump it in the database
    pid = os.getpid()
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    c.execute("UPDATE settings SET value = %s WHERE param = 'config.server.pid'" % pid)
    conn.commit()
    conn.close()

     # Override from Base class

    def on_message(self, timestamp, tags, channel, user, message):

        user = user   # usernames need to be all lowercase for this to work

        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)

        # We need to have the bot say something when the following sql statements have a value
        c.execute("SELECT * FROM channel_data WHERE toggle = 'y'")
        result = c.fetchall()
        for row in result:
            param = row['param']
            value = row['value']

            if param == 'follower':
                self.message(channel, 'Welcome to the channel, %s!!   500 Points have been awarded' % value)
                c.execute("UPDATE channel_data SET toggle = 'n' WHERE param = 'follower'")
                #conn.commit()
            elif param == 'hosts':
                self.message(channel, 'Thank you so much for that host, %s!!   100 Points Awarded!' % value)
                c.execute("UPDATE channel_data SET toggle = 'n' WHERE param = 'hosts'")
                #conn.commit()
            elif param == 'sub':
                self.message(channel, 'Thank you so much for that sub %s!!  500 Points awarded!' % value)
                c.execute("UPDATE channel_data SET toggle = 'n' WHERE param = 'sub'")
                #conn.commit()
        conn.commit()

        # Send notifications regarding keyword raffles

        c.execute("SELECT keyword, new from kwraffle_config WHERE new = 'Y' LIMIT 1")
        for row in c:
            raffle = row['keyword']
            # target = channel
            self.message(channel, "A new raffle has begun!  Type %s to enter!!!" % raffle)
            c.execute("UPDATE kwraffle_config SET new = 'N' WHERE keyword = '%s'" % raffle)
            conn.commit()

        # Announce winner
        c.execute("SELECT * FROM winners WHERE announce = 'y'")
        result = c.fetchone()
        if result:
            # target = channel
            keyword = result['keyword']
            username = result['username']
            self.message(channel, "Congratulations!  You have won @%s for the raffle %s !!!  SPEAK UP IN CHAT!!" % (
            username, keyword))
            c.execute(
                "UPDATE winners SET announce = 'n' WHERE username = '%s' AND keyword = '%s'" % (username, keyword))
            conn.commit()

        # Add user to raffle, if raffles are enabled
        if config['kwraffle'] == 'Online':

            # Get keyword from the message
            keyword = message.split()[0]

            if get_keyword(keyword) is True:

                # first check to make sure the user isn't already in the list
                c.execute(
                    "SELECT  kwraffle_config.keyword, kwraffle_config.active, raffle.keyword, raffle.username FROM raffle, kwraffle_config where raffle.username = '%s' AND raffle.keyword = '%s' AND kwraffle_config.active = 'y'" % (
                    user, keyword))
                result = c.fetchone()
                if result is None:
                    c.execute('INSERT INTO raffle (keyword, username) VALUES ("%s", "%s")' % (keyword, user))
                    conn.commit()
                    self.message(channel, "Recorded %s into keyword raffle %s!   Good Luck!!" % (user, keyword))

                else:
                    self.message(channel, "You have already been entered into this raffle @%s" % user)

        # Check to make sure the message does not contain a URL, if url checking is enabled
        extractor = URLExtract()
        urls = extractor.find_urls(message)
        if len(urls) == 1:
            conn = sql.connect(db_host, db_uname, db_pass, db_name)
            c = conn.cursor(sql.cursors.DictCursor)
            # First, we need to check to see if the url mod is enabled.
            c.execute("SELECT param, value FROM settings WHERE param = 'config.urlcheck' AND value = 'True'")
            row = c.fetchone()
            if row is not None:
                if regulars(user) is False and permits(user) is False and boteditors(user) is False and moderator(user) is False:
                    self.message(channel, "%s --> Ask for permission before posting a link!!! (PURGED!)" % user)
                    timeout = 5
                    self.message(channel, "/timeout %s %s" % (user, timeout))
                elif permits(user) is True:
                    c.execute("DELETE FROM permits WHERE username = '%s'" % user)
                    conn.commit()
                    conn.close()


        # Check to see if message is a command
        if message[0] == "!":
            # Check to see if it is a command or function
            if commands.is_valid_command(message.split(' ', 1)[0]):
                command = message

                if commands.check_returns_function(command.split(' ')[0]):
                    if commands.check_has_correct_args(command, command.split(' ')[0]):
                        args = command.split(' ')
                        del args[0]

                        command = command.split(' ')[0]

                        if commands.is_on_cooldown(command):
                            self.message(channel, 'Command is on cooldown. (%s) (%s) (%ss remaining)' % (command, user, int(commands.get_cooldown_remaining(command))))

                        else:
                            result = commands.pass_to_function(user, command, args)
                            commands.update_last_used(command)

                            if result:
                                self.message(channel, '%s' % result)

                else:
                    if commands.is_on_cooldown(command):
                        self.message(channel, 'Command is on cooldown. (%s) (%s) (%ss remaining)' % (command, user, int(commands.get_cooldown_remaining(command))))
                    elif commands.check_has_return(command):
                        commands.update_last_used(command)
                        self.message(channel, '%s' % (commands.get_return(command)))

    def on_cleared_chat(self, timestamp, tags, channel):
        self.message(channel, "Chat has been cleared by a moderator!!")

    def on_notice(self, timestamp, tags, channel, message):
        self.message(channel, "%s" % message)

    def on_usernotice(self, timestamp, tags, channel, message):
        self.message(channel, "%s" % message)




# First we need to modify the oauth key from the DB so that it works here
oauth = config['oauth_password'][6:]

client = Bot(botname, oauth).start()
client.handle_forever()


