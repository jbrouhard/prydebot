import pymysql as sql

from config.database import *

global config, db_name, db_host, db_uname, db_pass

settings = {}  # create new settings dictionary

conn = sql.connect(db_host, db_uname, db_pass, db_name)
c = conn.cursor(sql.cursors.DictCursor)

c.execute("Select * From settings")

for row in c:

    param = row['param']
    value = row['value']

    settings[param] = {}
    settings[param] = value

botname = settings.get('twitch.username', "")
oauth = settings.get('twitch.oauth', "")
channel = settings.get('twitch.channel', "")
twitchapi = settings.get('twitch.api.clientid', "")

kwraffle = settings.get('config.kwraffle.service', "")

points_timer = settings.get('config.points.timer', "")
points_event_timer = settings.get('points.event.timer', "")
points_sub_multiplier = settings.get('points.sub.miltiplier', "")
points_default = settings.get('config.points.default', "")
points_follower_award = settings.get('points.follower.award', "")
points_mod_multiplier = settings.get('points.mod.multiplier', "")
points_sub_award = settings.get('points.sub.award', "")
config_point_name = settings.get('config.point.name', "")
config_follow_points = settings.get('config.follow.points', "")
points_raider_award = settings.get('points.raider.award', "")
points_host_award = settings.get('points.host.award', "")

config_event_timer = settings.get('config.events.timer', "")
config_event_time = settings.get('config.event.time', "")
config_event_cooldown = settings.get('event.cooldown.timer', "")

config_api_timer = settings.get('config.api.timer', "")

config_songrequest = settings.get('config.songrequest', "")

config_google_api = settings.get('google.api', "")

config_bot_license = settings.get('bot.license', "")

config_bot_version = settings.get('bot.version', "")

se_token = settings.get('se.token', "")

se_key = settings.get('se.key', "")


config = {

    # details required to login to twitch IRC server
    'server': 'irc.chat.twitch.tv',
    'port': 6667,
    'botname': botname,
    'oauth_password': oauth,  # get this from http://twitchapps.com/tmi/
    'twitch.api.clientid': twitchapi,

    'channel': channel,

    # if set to true will display any data received
    'debug': True,

    # if set to true will log all messages from all channels
    'log_messages': True,

    # maximum amount of bytes to receive from socket - 1024-4096 recommended
    'socket_buffer_size': 2048,

    # keyword raffle settings - If set then bot will check for keyword raffles.
    'kwraffle': kwraffle,

    # Points configuration stuff here
    'points_timer': points_timer,
    'points_sub_multiplier': points_sub_multiplier,
    'points_default': points_default,
    'points_sub_award': points_sub_award,
    'points_follower_award': points_follower_award,
    'points_mod_multiplier': points_mod_multiplier,
    'point_name': config_point_name,
    'follow_points': config_follow_points,
    'points.raider.award': points_raider_award,
    'points.host.award': points_host_award,

    # Event timer
    'config_event_timer': config_event_timer,
    'config_event_cooldown': config_event_cooldown,


    # Twitch API Timer
    'config_api_timer': config_api_timer,

    # Bot License
    'config_bot_license': config_bot_license,

    # Bot Version
    'config_bot_version': config_bot_version,

    # StreamElements JWT token
    'se.token': se_token,
    'se.key': se_key,
}

conn.close()