#!/usr/bin/env python3

import schedule, time, logging, os
from lib.streamelementsapi import *
from lib.decapi import *


logging.basicConfig(filename='cron.log', filemode='w', level=logging.DEBUG, format='%(asctime)s %(message)s')


def get_pid():
    logging.info('Updating Cron PID')
    pid = os.getpid()
    try:
        conn = sql.connect(db_host, db_uname, db_pass, db_name)
        c = conn.cursor(sql.cursors.DictCursor)
        conn.ping(reconnect=True)
        c.execute("UPDATE settings SET value = %s WHERE param = 'config.cron.pid'" % pid)
        conn.commit()
        conn.close()
    except:
        return None


def job():
    ctime = time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())
    logging.info("I'm working...%s" % ctime)


# Points Timer for automatic point awarding
def points_timer():

    logging.info("Granting all viewers points")
    if config['points_timer'] is not None:
        try:
            conn = sql.connect(db_host, db_uname, db_pass, db_name)
            c = conn.cursor(sql.cursors.DictCursor)
            conn.ping(reconnect=True)
            ctime = time.time()
            timer = int(config['points_timer']) / 60
            points_timer = config['points_timer']
            cooldown = config_event_timer

            if int(float(ctime)) - int(float(cooldown)) >= int(float(points_timer)):

                mod_multiplier = config['points_mod_multiplier']

                points_default = config['points_default']

                channel1 = config['channel'][1:]

                # call twitch's API to get list of users in channel
                api = 'https://tmi.twitch.tv/group/user/' + channel1 + '/chatters'

                response = urlopen(api)
                readable = response.read().decode('utf-8')
                chatlist = loads(readable)
                chatters = chatlist['chatters']
                viewers = chatters['viewers']

                for i in viewers:
                    # Since viewers have no multiplier, just use points_default as points
                    pts = config['points_default']
                    # Check to make sure the user exists first
                    c.execute("SELECT user FROM points WHERE user = '%s'" % i)
                    result = c.fetchone()
                    if result is None:
                        c.execute("INSERT INTO points (user, points, time) VALUES ('%s', '%s', '%s')" % (i, pts, timer))
                        conn.commit()
                    else:  # Viewer exists, just update their point values.
                        c.execute("UPDATE points SET points = points + '%s', time = time + '%s' WHERE user = '%s'" % (pts, timer, i))
                        conn.commit()

                # call twitch's API to get list of users in channel
                api = 'https://tmi.twitch.tv/group/user/' + channel1 + '/chatters'

                response = urlopen(api)
                readable = response.read().decode('utf-8')
                chatlist = loads(readable)
                chatters = chatlist['chatters']
                mods = chatters['moderators']

                for i in mods:
                    # Since mods have a multiplier, we need to make the new points value
                    pts = int(points_default) * int(mod_multiplier)
                    # Check to make sure the user exists first
                    c.execute("SELECT user FROM points WHERE user = '%s'" % i)
                    result = c.fetchone()
                    if result is None:
                        c.execute("INSERT INTO points (user, points, time) VALUES ('%s', '%s', '%s')" % (i, pts, timer))
                        conn.commit()
                    else:  # Viewer     exists, just update their point values.
                        c.execute("UPDATE points SET user = '%s', points = points + '%s', time = time + '%s' WHERE user = '%s'" % (i, pts, timer, i))
                        conn.commit()

                # Update cooldown with new timer
                c.execute("UPDATE settings SET value = %s WHERE param = 'points.event.timer'" % ctime)
                conn.commit()
            conn.close()
        except:
            return None


def get_recent_follower():

    logging.info("Getting most recent follower")

    user = getrecentfollower()

    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute("SELECT * FROM channel_data WHERE param = 'follower' and value = '%s'" % user)
    result = c.fetchone()
    if result is None:
        c.execute("UPDATE channel_data SET value = '%s', toggle = 'y' WHERE param = 'follower'" % user)
        conn.commit()
    logging.info("Success getting recent follower!")

    conn.close()


def get_recent_host():

    logging.info("Getting most recent host")

    host = getrecenthost()

    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute('SELECT * FROM channel_data WHERE param = "hosts" and value = "%s"' % host)
    result = c.fetchone()
    if result is None:
        c.execute('UPDATE channel_data SET value = "%s", toggle = "y" WHERE param = "hosts"' % host)
        conn.commit()
    logging.info("Successfully updated latest host")

    conn.close()


def get_recent_sub():

    logging.info("Getting most recent sub")

    host = getrecentsub()

    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute('SELECT * FROM channel_data WHERE param = "sub" and value = "%s"' % host)
    result = c.fetchone()
    if result is None:
        c.execute('UPDATE channel_data SET value = "%s", toggle = "y" WHERE param = "sub"' % host)
        conn.commit()
    logging.info("Successfully updated latest subscriber")

    conn.close()


def get_uptime():

    logging.info("Getting current uptime")

    uptime = getuptime()

    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute('UPDATE channel_data SET value = "%s" WHERE param = "uptime"' % uptime)
    conn.commit()
    logging.info("Successfully updated uptime")

    conn.close()


def get_viewers():

    logging.info("Getting current viewer count")

    uptime = getViewrCount()

    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute('UPDATE channel_data SET value = "%s" WHERE param = "viewers"' % uptime)
    conn.commit()
    logging.info("Successfully updated viewer count")

    conn.close()


def get_totalsubs():

    logging.info("Getting current viewer count")

    subs = gettotalsubs()

    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute('UPDATE channel_data SET value = "%s" WHERE param = "total_subs"' % subs)
    conn.commit()
    logging.info("Successfully updated total subscriber count")

    conn.close()


def get_totalfollowers():

    logging.info("Getting current viewer count")

    subs = gettotalfollowers()

    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    c.execute('UPDATE channel_data SET value = "%s" WHERE param = "total_followers"' % subs)
    conn.commit()
    logging.info("Successfully updated total follower count")

    conn.close()

# schedule.every(1).minutes.do(job)
schedule.every(5).minutes.do(points_timer)
schedule.every(5).minutes.do(get_totalfollowers)
schedule.every(5).minutes.do(get_totalsubs)
schedule.every(5).seconds.do(get_viewers)
schedule.every(5).seconds.do(get_uptime)
schedule.every(5).seconds.do(get_recent_host)
schedule.every(5).seconds.do(get_recent_follower)
schedule.every(5).seconds.do(get_recent_sub)

schedule.every(1).seconds.do(get_pid)
# schedule.every(1).minutes.do(event_timer)
# schedule.every().hour.do(job)
# schedule.every().day.at("10:30").do(job)

while 1:
    # get_pid()
    # main()
    schedule.run_pending()
    time.sleep(1)
