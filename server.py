#!/usr/bin/env python3

from bot import *


def get_pid():
    logging.info('Updating Server PID')
    pid = os.getpid()
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    c.execute("UPDATE settings SET value = %s WHERE param = 'config.server.pid'" % pid)
    conn.commit()
    conn.close()


if __name__ == '__main__':
    get_pid()
    bot = Bot(config).run()


